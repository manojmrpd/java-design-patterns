package com.creditcard.model;

import com.creditcard.manager.AdsCreditCardManager;
import com.creditcard.manager.BamsCreditCardManager;
import com.creditcard.util.CreditCardInfo;

public class CorporateBankCreditCard implements CreditCard {

	@Override
	public CreditCardInfo createCreditCard() {
		
		CreditCardInfo cbcc = creditCardDetails();
		System.out.println(cbcc.getCreditCardName()+" || " +cbcc.getCreditCardNumber()+ " || "+cbcc.getUserName());
		return cbcc;
	}

	@Override
	public CreditCardInfo getCreditCard() {
		CreditCardInfo cbcc = creditCardDetails();
		String tokenNumber = BamsCreditCardManager.tokenize(cbcc);
		boolean authStatus = AdsCreditCardManager.authorize(tokenNumber);
		if(authStatus) {
			System.out.println("Authorization status is: "+authStatus);
			return creditCardDetails();
		} else {
			System.out.println("Authorization status is: "+authStatus);
			return new CreditCardInfo();
		}
	}
	
	private CreditCardInfo creditCardDetails() {
		CreditCardInfo creditCard = new CreditCardInfo();
		creditCard.setCreditCardName("cbcc");
		creditCard.setCreditCardNumber(51111111111L);
		creditCard.setUserName("jerry");
		return creditCard;
	}

}
