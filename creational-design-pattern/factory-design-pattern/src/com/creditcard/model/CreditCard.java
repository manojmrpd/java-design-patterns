package com.creditcard.model;

import com.creditcard.util.CreditCardInfo;

public interface CreditCard {
	
     CreditCardInfo createCreditCard();
	 CreditCardInfo getCreditCard();

}
