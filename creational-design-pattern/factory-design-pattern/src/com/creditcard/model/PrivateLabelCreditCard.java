package com.creditcard.model;

import com.creditcard.manager.BamsCreditCardManager;
import com.creditcard.util.CreditCardInfo;

public class PrivateLabelCreditCard implements CreditCard {

	@Override
	public CreditCardInfo createCreditCard() {
		CreditCardInfo plcc = createCreditCardInfo();
		System.out.println(plcc.getCreditCardName()+" || " +plcc.getCreditCardNumber()+ " || "+plcc.getUserName());
		return plcc;		
	}


	@Override
	public CreditCardInfo getCreditCard() {
		CreditCardInfo plcc = createCreditCardInfo();
		String tokenNumber = BamsCreditCardManager.tokenize(plcc);
		boolean authStatus = BamsCreditCardManager.authorize(tokenNumber);
		if(authStatus) {
			System.out.println("Authorization status is: "+authStatus);
			return createCreditCardInfo();
		} else {
			System.out.println("Authorization status is: "+authStatus);
			return new CreditCardInfo();
		}
	}
	
	private CreditCardInfo createCreditCardInfo() {
		CreditCardInfo creditCard = new CreditCardInfo();
		creditCard.setCreditCardName("plcc");
		creditCard.setCreditCardNumber(41111111111L);
		creditCard.setUserName("tom");
		return creditCard;
	}
}
