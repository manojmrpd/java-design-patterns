package com.creditcard.factory;

import com.creditcard.model.CorporateBankCreditCard;
import com.creditcard.model.CreditCard;
import com.creditcard.model.PrivateLabelCreditCard;
import com.creditcard.util.Constants;

public class CreditCardFactory {

	public CreditCard createCreditCard(String creditCardType) {
		if (null != creditCardType && !creditCardType.isEmpty() && 
				creditCardType.equalsIgnoreCase(Constants.PLCC)) {
			return new PrivateLabelCreditCard();
		} else if (null != creditCardType && !creditCardType.isEmpty() && 
				creditCardType.equalsIgnoreCase(Constants.CBCC)) {
			return new CorporateBankCreditCard();
		} else {
			return null;
		}
	}

}
