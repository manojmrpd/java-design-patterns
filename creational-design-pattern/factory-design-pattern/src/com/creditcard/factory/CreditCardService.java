package com.creditcard.factory;

import com.creditcard.model.CreditCard;
import com.creditcard.util.Constants;

public class CreditCardService {

	public static void main(String[] args) {
		
		
		CreditCardFactory factory = new CreditCardFactory();
		CreditCard plcc = factory.createCreditCard(Constants.PLCC);
		plcc.createCreditCard();
		plcc.getCreditCard();
		CreditCard cbcc = factory.createCreditCard(Constants.CBCC);
		cbcc.createCreditCard();
		cbcc.getCreditCard();
	}

}
