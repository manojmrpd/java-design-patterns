package com.notification.model;

public class EmailNotification implements Notification {

	@Override
	public void notifyUser() {
		System.out.println("Send Email notification to user email address");
	}

}
