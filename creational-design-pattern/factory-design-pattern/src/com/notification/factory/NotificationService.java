package com.notification.factory;

import com.notification.model.Notification;

public class NotificationService {

	public static void main(String[] args) {
		
		NotificationFactory factory = new NotificationFactory();
		Notification sms = factory.createNotification("SMS");
		Notification email = factory.createNotification("EMAIL");
	
		sms.notifyUser();
		email.notifyUser();
	}
}
