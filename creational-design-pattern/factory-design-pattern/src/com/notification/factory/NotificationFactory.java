package com.notification.factory;

import com.notification.model.EmailNotification;
import com.notification.model.Notification;
import com.notification.model.SMSNotification;

public class NotificationFactory {
	
	@SuppressWarnings("null")
	public Notification createNotification(String channel){
		
		if(null == channel && channel.isEmpty()) {
			throw new NullPointerException();
		} else if (channel.equalsIgnoreCase("SMS")) {
			return new SMSNotification();
		} else if (channel.equalsIgnoreCase("EMAIL")) {
			return new EmailNotification();
		}
		return null;
	}
}
