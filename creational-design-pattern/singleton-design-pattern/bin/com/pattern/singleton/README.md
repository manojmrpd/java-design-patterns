In this section, we will learn different approaches of Singleton pattern implementation and design concerns with the implementation.
1.	Eager initialization
2.	Static block initialization
3.	Lazy Initialization
4.	Thread Safe Singleton
5.	Bill Pugh Singleton Implementation
6.	Using Reflection to destroy Singleton Pattern
7.	Enum Singleton
8.	Serialization and Singleton

# 1. Eager initialization
In eager initialization, the instance of Singleton Class is created at the time of class loading, this is the easiest method to create a singleton class but it has a drawback that instance is created even though client application might not be using it.
Here is the implementation of the static initialization singleton class.
## Program to illustrate eager initialization
```java 
package com.pattern.singleton.eager;

class Singleton {

	private static Singleton instance = new Singleton();

	private Singleton() {
	}

	public static Singleton getInstance() {
		return instance;
	}
}

public class EagerInitializedSingleton {

	public static void main(String[] args) {

		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" + s1.hashCode());
		System.out.println("instanceTwo hashCode=" + s2.hashCode());
		if (s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```

If your singleton class is not using a lot of resources, this is the approach to use. But in most of the scenarios, Singleton classes are created for resources such as File System, Database connections, etc. We should avoid the instantiation until unless client calls the getInstance method. Also, this method doesn’t provide any options for exception handling.
# 2. Static block initialization
Static block initialization implementation is similar to eager initialization, except that instance of class is created in the static block that provides option for exception handling.
Both eager initialization and static block initialization creates the instance even before it’s being used and that is not the best practice to use. So in further sections, we will learn how to create a Singleton class that supports lazy initialization.
```java 
package com.pattern.singleton.staticblock;

class Singleton {

	private static Singleton instance;

	private Singleton() {
	}

	static {
		try {
			if (instance == null) {
				instance = new Singleton();
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception occured in creating singleton instance");
		}
	}

	public static Singleton getInstance() {
		return instance;
	}

}

public class StaticBlockSingleton {

	public static void main(String[] args) {
		
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" +s1.hashCode());
		System.out.println("instanceTwo hashCode=" +s2.hashCode());
		if(s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```
# 3. Lazy Initialization
Lazy initialization method to implement Singleton pattern creates the instance in the global access method. Here is the sample code for creating Singleton class with this approach.
The below implementation works fine in case of the single-threaded environment but when it comes to multithreaded systems, it can cause issues if multiple threads are inside the if condition at the same time. It will destroy the singleton pattern and both threads will get the different instances of the singleton class. In next section, we will see different ways to create a thread-safe singleton class.
```java 
package com.pattern.singleton.lazy;

class Singleton {
	
	 private static Singleton instance;
	    
	    private Singleton(){}
	    
	    public static Singleton getInstance(){
	        if(instance == null){
	            instance = new Singleton();
	        }
	        return instance;
	    }
}
public class LazyInitializedSingleton {
	
	public static void main(String[] args) {
		
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" +s1.hashCode());
		System.out.println("instanceTwo hashCode=" +s2.hashCode());
		if(s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```
# 4. Thread Safe Singleton
The easier way to create a thread-safe singleton class is to make the global access method synchronized, so that only one thread can execute this method at a time. General implementation of this approach is like the below class.
```java 
package com.pattern.singleton.threadsafe;

class Singleton {
	
	private static Singleton instance;
	
	private Singleton() {}
	
	public static synchronized Singleton getInstance() {
		if(null==instance) {
			instance = new Singleton();
		}
		return instance;
	}
}

public class ThreadSafeSingleton {

	public static void main(String[] args) {
		
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" +s1.hashCode());
		System.out.println("instanceTwo hashCode=" +s2.hashCode());
		if(s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```
Above implementation works fine and provides thread-safety but it reduces the performance because of the cost associated with the synchronized method, although we need it only for the first few threads who might create the separate instances (Read: Java Synchronization). 

# 5. Double Checked Locking Singleton
To avoid this extra overhead every time, double checked locking principle is used. In this approach, the synchronized block is used inside the if condition with an additional check to ensure that only one instance of a singleton class is created.
The following code snippet provides the double-checked locking implementation.
```java 
package com.pattern.singleton.threadsafe.doublelocking;

class Singleton {

	private static Singleton instance;

	private Singleton() {}

	public static Singleton getInstance() {
		if (null == instance) {
			synchronized (Singleton.class) {
				if (null == instance) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
}

public class DoubeLockingSingleton {

	public static void main(String[] args) {
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" +s1.hashCode());
		System.out.println("instanceTwo hashCode=" +s2.hashCode());
		if(s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```
# 6. Bill Pugh Singleton Implementation
Prior to Java 5, java memory model had a lot of issues and the above approaches used to fail in certain scenarios where too many threads try to get the instance of the Singleton class simultaneously. So Bill Pugh came up with a different approach to create the Singleton class using an inner static helper class. The Bill Pugh Singleton implementation goes like this;
```java 
package com.pattern.singleton.billpugh;

class Singleton {

	private Singleton() {
	}

	private static class InnerClass {
		private static final Singleton INSTANCE = new Singleton();
	}

	public static Singleton getInstance() {
		return InnerClass.INSTANCE;
	}

}

public class BillPughSingleton {

	public static void main(String[] args) {
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" + s1.hashCode());
		System.out.println("instanceTwo hashCode=" + s2.hashCode());
		if (s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}

}
```
## Console Output
```bash
instanceOne hashCode=366712642
instanceTwo hashCode=366712642
Both Instances Hashcode is same. Hence It is a Singleton object
```
Notice the private inner static class that contains the instance of the singleton class. When the singleton class is loaded, SingletonHelper class is not loaded into memory and only when someone calls the getInstance method, this class gets loaded and creates the Singleton class instance.
This is the most widely used approach for Singleton class as it doesn’t require synchronization. I am using this approach in many of my projects and it’s easy to understand and implement also.
# 7. Serialization Problem with Singleton
Sometimes in distributed systems, we need to implement Serializable interface in Singleton class so that we can store its state in the file system and retrieve it at a later point of time. Here is a small singleton class that implements Serializable interface also.
The problem with serialized singleton class is that whenever we deserialize it, it will create a new instance of the class. Let’s see it with a simple program.
```java 
package com.pattern.singleton.serializationproblem;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class SerializedSingleton implements Serializable {

	private static final long serialVersionUID = 8342983105307128608L;

	private SerializedSingleton() {
	}

	private static class InnerClass {
		private static final SerializedSingleton INSTANCE = new SerializedSingleton();
	}

	public static SerializedSingleton getInstance() {
		return InnerClass.INSTANCE;
	}
}

public class SerializationSingletonProblem {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		SerializedSingleton instanceOne = SerializedSingleton.getInstance();
		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("filename.ser"));
		out.writeObject(instanceOne);
		out.close();

		// deserialize from file to object
		ObjectInput in = new ObjectInputStream(new FileInputStream("filename.ser"));
		SerializedSingleton instanceTwo = (SerializedSingleton) in.readObject();
		in.close();

		System.out.println("instanceOne hashCode=" + instanceOne.hashCode());
		System.out.println("instanceTwo hashCode=" + instanceTwo.hashCode());
		if (instanceOne.equals(instanceTwo)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Both instance hashcode are not same. Hence it is not a Singleton object");
		}

	}
}
```
## Console Output
```bash
instanceOne hashCode=865113938
instanceTwo hashCode=2003749087
Both instance hashcode are not same. Hence it is not a Singleton object
```
