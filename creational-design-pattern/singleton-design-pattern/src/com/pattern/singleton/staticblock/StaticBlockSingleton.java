package com.pattern.singleton.staticblock;

class Singleton {

	private static Singleton instance;

	private Singleton() {
	}

	static {
		try {
			if (instance == null) {
				instance = new Singleton();
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception occured in creating singleton instance");
		}
	}

	public static Singleton getInstance() {
		return instance;
	}

}

public class StaticBlockSingleton {

	public static void main(String[] args) {
		
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" +s1.hashCode());
		System.out.println("instanceTwo hashCode=" +s2.hashCode());
		if(s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
