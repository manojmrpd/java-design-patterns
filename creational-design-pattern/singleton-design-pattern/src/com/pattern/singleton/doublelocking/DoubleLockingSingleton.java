package com.pattern.singleton.doublelocking;

class Singleton {

	private static Singleton instance;

	private Singleton() {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			synchronized (Singleton.class) {
				if (instance == null) {
					instance = new Singleton();
				}
			}
		}
		return instance;
	}
}

public class DoubleLockingSingleton {

	public static void main(String[] args) {
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("instanceOne hashCode=" + s1.hashCode());
		System.out.println("instanceTwo hashCode=" + s2.hashCode());
		if (s1.equals(s2)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Its Not a Singleton object");
		}
	}
}
