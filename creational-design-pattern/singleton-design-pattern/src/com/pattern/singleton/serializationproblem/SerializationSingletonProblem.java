package com.pattern.singleton.serializationproblem;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class SerializedSingleton implements Serializable {

	private static final long serialVersionUID = 8342983105307128608L;

	private SerializedSingleton() {
	}

	private static class InnerClass {
		private static final SerializedSingleton INSTANCE = new SerializedSingleton();
	}

	public static SerializedSingleton getInstance() {
		return InnerClass.INSTANCE;
	}
}

public class SerializationSingletonProblem {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		SerializedSingleton instanceOne = SerializedSingleton.getInstance();
		ObjectOutput out = new ObjectOutputStream(new FileOutputStream("filename.ser"));
		out.writeObject(instanceOne);
		out.close();

		// deserialize from file to object
		ObjectInput in = new ObjectInputStream(new FileInputStream("filename.ser"));
		SerializedSingleton instanceTwo = (SerializedSingleton) in.readObject();
		in.close();

		System.out.println("instanceOne hashCode=" + instanceOne.hashCode());
		System.out.println("instanceTwo hashCode=" + instanceTwo.hashCode());
		if (instanceOne.equals(instanceTwo)) {
			System.out.println("Both Instances Hashcode is same. Hence It is a Singleton object");
		} else {
			System.out.println("Both instance hashcode are not same. Hence it is not a Singleton object");
		}

	}
}
