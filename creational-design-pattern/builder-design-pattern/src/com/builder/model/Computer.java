package com.builder.model;

public class Computer {
	
	//Required
	private String hdd;
	private String ram;
	//Optional
	private boolean isGraphicsEnabled;
	private boolean isBluetoothEnabled;
	
	public String getHdd() {
		return hdd;
	}
	public String getRam() {
		return ram;
	}
	public boolean isGraphicsEnabled() {
		return isGraphicsEnabled;
	}
	public boolean isBluetoothEnabled() {
		return isBluetoothEnabled;
	}
	
	private Computer(ComputerBuilder builder) {
		this.hdd=builder.hdd;
		this.ram=builder.ram;
		this.isBluetoothEnabled=builder.isBluetoothEnabled;
		this.isGraphicsEnabled=builder.isGraphicsEnabled;
	}
	
	public static class ComputerBuilder {
		
		private String hdd;
		private String ram;
		private boolean isGraphicsEnabled;
		private boolean isBluetoothEnabled;
		
		public ComputerBuilder(String hdd, String ram) {
			super();
			this.hdd = hdd;
			this.ram = ram;
		}

		public ComputerBuilder setGraphicsEnabled(boolean isGraphicsEnabled) {
			this.isGraphicsEnabled = isGraphicsEnabled;
			return this;
		}

		public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
			this.isBluetoothEnabled = isBluetoothEnabled;
			return this;
		}
		
		public Computer build() {
			return new Computer(this);
		}
		
	}
}
