package com.builder.model;

public class ComputerService {

	public static void main(String[] args) {
		Computer comp = new Computer.ComputerBuilder("500gb", "2gb").setBluetoothEnabled(true).setGraphicsEnabled(false).build();
		System.out.println(comp.getHdd());
		System.out.println(comp.getRam());
		System.out.println(comp.isGraphicsEnabled());
		System.out.println(comp.isBluetoothEnabled());
	}
}
