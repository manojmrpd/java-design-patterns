package com.prototype.example2;

import java.util.ArrayList;
import java.util.List;

public class Employee implements Cloneable {
	
	private List<String> employees;

	public Employee(List<String> list) {
		super();
		this.employees = list;
	}

	public Employee() {
		employees = new ArrayList<>();
	}

	public List<String> getEmployees() {
		return employees;
	}

	public void setEmployees(List<String> employees) {
		this.employees = employees;
	}
	
	public void loadData() {
		employees.add("Jeff");
		employees.add("Gene");
		employees.add("Cath");
	}

	@Override
	public String toString() {
		return "Employee [employees=" + employees + "]";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		List<String> temp = new ArrayList<>();
		for(String s: this.getEmployees()) {
			temp.add(s);
		}
		return new Employee(temp);
	}
	
	
	
	
	
	
	

}
