package com.prototype.example2;

import java.util.List;

public class PrototypeService {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		Employee emp = new Employee();
		emp.loadData();
		System.out.println(emp.toString());
		
		Employee emp1 = (Employee) emp.clone();
		List<String> list1 = emp1.getEmployees();
		list1.remove("Cath");
		System.out.println(emp1.toString());
		
		Employee emp2 = (Employee) emp.clone();
		List<String> list2 = emp2.getEmployees();
		list2.add("Wilson");
		System.out.println(emp2.toString());
		
	}
}
