package com.prototype.example1;

import java.util.List;

public class CompanyService {

	public static void main(String[] args) throws CloneNotSupportedException {
		Company infosys = new Company();
		infosys.loadData();
		infosys.setCompany("infosys");
		System.out.println(infosys.toString());
		
		Company cogni  = infosys.clone();
		cogni.setCompany("cognizant");
		List<Employee> cogniList = cogni.getEmployees();
		cogniList.remove(0);
		System.out.println(cogni.toString());
		
		Company wirpo  = infosys.clone();
		wirpo.setCompany("wipro");
		List<Employee> wiproList = wirpo.getEmployees();
		wiproList.remove(1);
		System.out.println(wirpo.toString());
	}
}
