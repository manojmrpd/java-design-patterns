package com.prototype.example1;

import java.util.ArrayList;
import java.util.List;

public class Company implements Cloneable {
	
	private List<Employee> employees = new ArrayList<>();
	private String company;
	
	public final List<Employee> getEmployees() {
		return employees;
	}
	public final void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public final String getCompany() {
		return company;
	}
	public final void setCompany(String company) {
		this.company = company;
	}
	
	public Company(List<Employee> employees) {
		this.employees = employees;
	}
	public Company() {
	}
	
	public void loadData() {
		for(int i=1;i<=10;i++) {
			Employee e = new Employee();
			e.setEmpId(i);
			e.setEmpName("Employee-"+i);
			employees.add(e);
		}
	}
	@Override
	protected Company clone() throws CloneNotSupportedException {
		List<Employee> temp = new ArrayList<>();
		for(Employee e: this.getEmployees()) {
			temp.add(e);
		}
		return new Company(temp);
	}
	@Override
	public String toString() {
		return "Company [employees=" + employees + ", company=" + company + "]";
	}
	
	
	
	
	
	
}
