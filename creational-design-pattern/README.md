
# Creational Design Pattern

Creational design patterns provide solution to instantiate an object in the best possible way for specific situations.
* Singleton Pattern
* Factory Pattern
* Abstract Factory Pattern
* Builder Pattern
* Prototype Pattern

## 1. Singleton Pattern
* Singleton pattern restricts the instantiation of a class and ensures that only one instance of the class exists through out the application in the Java virtual machine. It seems to be a very simple design pattern but when it comes to implementation, it comes with a lot of implementation concerns.
* Although the Singleton pattern was introduced by GoF, the original implementation is known to be problematic in multithreaded scenarios.
* So here, we're going to follow a more optimal approach that makes use of a static inner class:
```java 
public class Singleton  {    
    private Singleton() {}
    
    private static class SingletonHolder {    
        public static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {    
        return SingletonHolder.instance;    
    }
}
```
* Here, we've created a static inner class that holds the instance of the Singleton class. It creates the instance only when someone calls the getInstance() method and not when the outer class is loaded.
* This is a widely used approach for a Singleton class as it doesn’t require synchronization, is thread safe, enforces lazy initialization and has comparatively less boilerplate.
* Also, note that the constructor has the private access modifier. This is a requirement for creating a Singleton since a public constructor would mean anyone could access it and start creating new instances.

### When to Use Singleton Design Pattern
* For resources that are expensive to create (like database connection objects)
* It's good practice to keep all loggers as Singletons which increases performance
* Classes which provide access to configuration settings for the application
* Classes that contain resources that are accessed in shared mode
## 2. Factory Pattern
* The factory design pattern is used when we have a superclass with multiple sub-classes and based on input, we need to return one of the sub-classes. This pattern takes out the responsibility of the instantiation of a class from the client program to the factory class. 
* The Factory Design Pattern or Factory Method Design Pattern is one of the most used design patterns in Java.
* According to GoF, this pattern “defines an interface for creating an object, but let subclasses decide which class to instantiate. The Factory method lets a class defer instantiation to subclasses”.
* This pattern delegates the responsibility of initializing a class from the client to a particular factory class by creating a type of virtual constructor.
* To achieve this, we rely on a factory which provides us with the objects, hiding the actual implementation details. The created objects are accessed using a common interface.

In this example, we'll create a Polygon interface which will be implemented by several concrete classes. A PolygonFactory will be used to fetch objects from this family:

![image](https://www.baeldung.com/wp-content/uploads/2017/11/Factory_Method_Design_Pattern.png)

Let's first create the Polygon interface:
```java
public interface Polygon {
    String getType();
}
```
Next, we'll create a few implementations like Square, Triangle, etc. that implement this interface and return an object of Polygon type.

Now we can create a factory that takes the number of sides as an argument and returns the appropriate implementation of this interface:
```java
public class PolygonFactory {
    public Polygon getPolygon(int numberOfSides) {
        if(numberOfSides == 3) {
            return new Triangle();
        }
        if(numberOfSides == 4) {
            return new Square();
        }
        if(numberOfSides == 5) {
            return new Pentagon();
        }
        if(numberOfSides == 7) {
            return new Heptagon();
        }
        else if(numberOfSides == 8) {
            return new Octagon();
        }
        return null;
    }
}
```
Notice how the client can rely on this factory to give us an appropriate Polygon, without having to initialize the object directly.
### When to Use Factory Method Design Pattern
* When the implementation of an interface or an abstract class is expected to change frequently
* When the current implementation cannot comfortably accommodate new change
* When the initialization process is relatively simple, and the constructor only requires a handful of parameters

## 3. Abstract Factory Pattern
* Abstract Factory pattern is similar to Factory pattern and it’s a factory of factories. If you are familiar with the factory design pattern in java, you will notice that we have a single Factory class that returns the different sub-classes based on the input provided and the factory class uses if-else or switch statements to achieve this.
* In Abstract Factory pattern, we get rid of if-else block and have a factory class for each sub-class and then an Abstract Factory class that will return the sub-class based on the input factory class. Check out Abstract Factory Pattern to know how to implement this pattern with example program.
* Abstract Factory “provides an interface for creating families of related or dependent objects without specifying their concrete classes”. In other words, this model allows us to create objects that follow a general pattern.
* An example of the Abstract Factory design pattern in the JDK is the newInstance() of javax.xml.parsers.DocumentBuilderFactory class.

In this example, we'll create two implementations of the Factory Method Design pattern: AnimalFactory and ColorFactory.
After that, we'll manage access to them using an Abstract Factory AbstractFactory:
![image](https://www.baeldung.com/wp-content/uploads/2018/11/updated_abstract_factory.jpg)
First, we'll create a family of Animal class and will, later on, use it in our Abstract Factory.

Here's the Animal interface:
```java
public interface Animal {
    String getAnimal();
    String makeSound();
}
```
and a concrete implementation Duck:
```java
public class Duck implements Animal {

    @Override
    public String getAnimal() {
        return "Duck";
    }

    @Override
    public String makeSound() {
        return "Squeks";
    }
}
```
* Furthermore, we can create more concrete implementations of Animal interface (like Dog, Bear, etc.) exactly in this manner.
* The Abstract Factory deals with families of dependent objects. With that in mind, we're going to introduce one more family Color as an interface with a few implementations (White, Brown,…).
* We'll skip the actual code for now, but it can be found here.
* Now that we've got multiple families ready, we can create an AbstractFactory interface for them
```java
public interface AbstractFactory<T> {
    T create(String animalType) ;
}
```
Next, we'll implement an AnimalFactory using the Factory Method design pattern that we discussed in the previous section:
```java
public class AnimalFactory implements AbstractFactory<Animal> {

    @Override
    public Animal create(String animalType) {
        if ("Dog".equalsIgnoreCase(animalType)) {
            return new Dog();
        } else if ("Duck".equalsIgnoreCase(animalType)) {
            return new Duck();
        }
        return null;
    }
}
```
* Similarly, we can implement a factory for the Color interface using the same design pattern.
* When all this is set, we'll create a FactoryProvider class that will provide us with an implementation of AnimalFactory or ColorFactory depending on the argument that we supply to the getFactory() method:
```java
public class FactoryProvider {
    public static AbstractFactory getFactory(String choice){
        
        if("Animal".equalsIgnoreCase(choice)){
            return new AnimalFactory();
        }
        else if("Color".equalsIgnoreCase(choice)){
            return new ColorFactory();
        }
        return null;
    }
}
```
### When to Use Abstract Factory Pattern:
* The client is independent of how we create and compose the objects in the system
* The system consists of multiple families of objects, and these families are designed to be used together
* We need a run-time value to construct a particular dependency
* While the pattern is great when creating predefined objects, adding the new ones might be challenging. To support the new type of objects will require changing the AbstractFactory class and all of its subclasses.

## 4. Builder Pattern
* The Builder Design Pattern is another creational pattern designed to deal with the construction of comparatively complex objects.
* This pattern was introduced to solve some of the problems with Factory and Abstract Factory design patterns when the Object contains a lot of attributes. Builder pattern solves the issue with a large number of optional parameters and inconsistent state by providing a way to build the object step-by-step and provide a method that will actually return the final Object.
* When the complexity of creating object increases, the Builder pattern can separate out the instantiation process by using another object (a builder) to construct the object.
* The original Builder Design Pattern introduced by GoF focuses on abstraction and is very good when dealing with complex objects, however, the design is a little complicated.
This example has only one class, BankAccount which contains a builder as a static inner class:
```java
public class BankAccount {
    
    private String name;
    private String accountNumber;
    private String email;
    private boolean newsletter;

    // constructors/getters
    
    public static class BankAccountBuilder {
        // builder code
    }
}
```
* Note that all the access modifiers on the fields are declared private since we don't want outer objects to access them directly.
* The constructor is also private so that only the Builder assigned to this class can access it. All of the properties set in the constructor are extracted from the builder object which we supply as an argument.
We've defined BankAccountBuilder in a static inner class:

```java
public static class BankAccountBuilder {
    
    private String name;
    private String accountNumber;
    private String email;
    private boolean newsletter;
    
    public BankAccountBuilder(String name, String accountNumber) {
        this.name = name;
        this.accountNumber = accountNumber;
    }

    public BankAccountBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public BankAccountBuilder wantNewsletter(boolean newsletter) {
        this.newsletter = newsletter;
        return this;
    }
    
    public BankAccount build() {
        return new BankAccount(this);
    }
}
```
* Notice we've declared the same set of fields that the outer class contains. Any mandatory fields are required as arguments to the inner class's constructor while the remaining optional fields can be specified using the setter methods.
* This implementation also supports the fluent design approach by having the setter methods return the builder object.
* Finally, the build method calls the private constructor of the outer class and passes itself as the argument. The returned BankAccount will be instantiated with the parameters set by the BankAccountBuilder.

Let's see a quick example of the builder pattern in action:
```java
BankAccount newAccount = new BankAccount
  .BankAccountBuilder("Jon", "22738022275")
  .withEmail("jon@example.com")
  .wantNewsletter(true)
  .build();
  ```
  ### When to Use Builder Pattern
* When the process involved in creating an object is extremely complex, with lots of mandatory and optional parameters
* When an increase in the number of constructor parameters leads to a large list of constructors
* When client expects different representations for the object that's constructed

## 5. Prototype Pattern
* The prototype pattern is used when the Object creation is a costly affair and requires a lot of time and resources and you have a similar object already existing. So this pattern provides a mechanism to copy the original object to a new object and then modify it according to our needs. This pattern uses java cloning to copy the object.
* Prototype design pattern mandates that the Object which you are copying should provide the copying feature. It should not be done by any other class. However whether to use the shallow or deep copy of the Object properties depends on the requirements and it’s a design decision.
* Prototype allows us to hide the complexity of making new instances from the client. The concept is to copy an existing object rather than creating a new instance from scratch, something that may include costly operations. The existing object acts as a prototype and contains the state of the object. The newly copied object may change same properties only if required. This approach saves costly resources and time, especially when object creation is a heavy process.
Let's see a quick example of the prototype pattern in action:
```java
package com.designpattern.creational.prototype;
import java.util.ArrayList;
import java.util.List;

public class Employees implements Cloneable{

	private List<String> empList;
	
	public Employees(){
		empList = new ArrayList<String>();
	}
	
	public Employees(List<String> list){
		this.empList=list;
	}
	public void loadData(){
		//read all employees from database and put into the list
		empList.add("Manoj");
		empList.add("Ashok");
		empList.add("Aaradhya");
		empList.add("Ashwini");
	}
	
	public List<String> getEmpList() {
		return empList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException{
			List<String> temp = new ArrayList<String>();
			for(String s : this.getEmpList()){
				temp.add(s);
			}
			return new Employees(temp);
	}
}
```
Notice that the clone method is overridden to provide a deep copy of the employees list.

Here is the prototype design pattern example test program that will show the benefit of prototype pattern.
```java
package com.designpattern.creational.prototype;
import java.util.List;
import com.designpattern.creational.prototype.Employees;

public class PrototypePatternTest {

	public static void main(String[] args) throws CloneNotSupportedException {
		Employees emps = new Employees();
		emps.loadData();
		
		//Use the clone method to get the Employee object
		Employees empsNew = (Employees) emps.clone();
		Employees empsNew1 = (Employees) emps.clone();
		List<String> list = empsNew.getEmpList();
		list.add("Swaroopa");
		List<String> list1 = empsNew1.getEmpList();
		list1.remove("Rakesh");
		
		System.out.println("emps List: "+emps.getEmpList());
		System.out.println("empsNew List: "+list);
		System.out.println("empsNew1 List: "+list1);
	}
}
```
If the object cloning was not provided, we will have to make database call to fetch the employee list every time. Then do the manipulations that would have been resource and time consuming.
